:: Transfer Agent for AirProducts
:: Transfer method: POST
:: When is delivery reported: When POST is successful
::
:: Syntax
:: ScriptName FileToTransfer
::
:: Returns 0 if successful, 
::         1 if called incorrectly (no parameter)
::         2 if file does not exist,
::         3 if file exists but not encrypted, transferred or some other error
::


:: Report start
echo Transfer starting


:: A debugging aid
echo Parameters are XX %1 XX %2 XX %3 XX


:: Check program called correctly
if "x%1x"=="xx" goto ERROR1


:: Standard variables
set CLIENTROOT=T:\BFP\clients\AAC420179913
:: Use this to point to config or temporary files
:: Temp files should always be in the client's root folder

:: TEST VLT
::set LOCALX=T:\BfP\clients\AAC420179913\test_output


:: Get parameters
set FULLFILENAME=%~f1
set DRIVE=%~d1
set PATHNAME=%~p1
set FILENAME=%~n1
set EXTENSION=%~x1


:: Check the file exists
if not exist "%FULLFILENAME%" goto ERROR2


:: Transfer file, error if fails
T:\BFP\utilities\CLEMAIL\clemail.exe -smtpserver mail.internal.ob10.com -to leanna.ahmad@tungsten-network.com -from systems-support@system.tungsten-network.com -subject "Cargill Test files" -body "Cargill Test files" -encoding "base64" -printmime || goto ERROR3
::T:\BFP\utilities\cURL\bins\curl.exe --insecure --data-binary @%FULLFILENAME% --header "Content-Type: text/xml" "https://service-2.ariba.com/service/transaction/cxml.asp" --trace-ascii - | findstr /C:"Accepted" || goto ERROR3
T:\BfP\utilities\cURL_AB8\bins\curl.exe --insecure --data-binary "@%FULLFILENAME%" --header "Content-Type: text/xml" "https://service-2.ariba.com/service/transaction/cxml.asp"  --trace-ascii - | findstr /C:"Status code" || goto ERROR3
::T:\BFP\utilities\cURL7.50.3\bin\curl.exe --insecure --data-binary @%FULLFILENAME% --header "Content-Type: text/xml" "https://service-2.ariba.com/service/transaction/cxml.asp" --trace-ascii - | findstr /C:"Status code" || goto ERROR3 
::copy /y "%FULLFILENAME%" "%LOCALX%" || goto ERROR3
goto SUCCESS

:SUCCESS
:: Report
echo File transfer successful
:: exit
exit /b 0


:ERROR1
:: Report
echo TransferAgent called incorrectly
:: exit
exit /b 1


:ERROR2
:: Report
echo File does not exist
:: exit
exit /b 2


:ERROR3
:: Report
echo File transfer failed
:: exit
exit /b 3

